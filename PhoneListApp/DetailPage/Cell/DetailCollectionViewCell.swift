//
//  DetailCollectionViewCell.swift
//  PhoneListApp
//
//  Created by faiq adi on 14/03/23.
//

import Foundation
import UIKit

class DetailCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
           super.init(frame: frame)

           addViews()
       }
    
    required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    
    func addViews(){
        addSubview(iconImage)
        
        NSLayoutConstraint.activate([
            iconImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            iconImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            iconImage.topAnchor.constraint(equalTo: topAnchor),
            iconImage.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
    }
    
    let iconImage: UIImageView = {
        let image = UIImageView()
        image.programatically()
        image.backgroundColor = .lightGray
        return image
    }()
}



