//
//  File.swift
//  PhoneListApp
//
//  Created by faiq adi on 14/03/23.
//

import Foundation
import UIKit
import Kingfisher

class DetailPageVC: DetailPageVCBuilder{
    
    var imageUrl = ""
    var descData = ""
    var titleData = ""
    
    var iconImages = [String]()
    var productData : Product!
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setData()
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let useLocalStorageAction = UITapGestureRecognizer(target: self, action: #selector(setLocalData))
        saveCircle.addGestureRecognizer(useLocalStorageAction)
        saveCircle.isUserInteractionEnabled = true
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = true
    }
    @objc func backAction(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func setLocalData(){
//        UserDefaults.standard.setArrayStringValue(value: arrayResult, identifier: .result)
        let labelAlert = UILabel()
        self.showToast(labelAlert, message: "Data Saved as Favorite", constaint: [
            labelAlert.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10)
        ])
    }
    
    func setData(){
        let urlImage = URL(string: imageUrl)!
        let resource = ImageResource(downloadURL: urlImage, cacheKey: "imageCell")
        
        thumbnailImage.kf.setImage(with: resource)
        detailTitle.text = titleData
        detailDesc.text = descData
    }
}
extension DetailPageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return iconImages.count
       }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DetailCollectionViewCell
           let urlImage = URL(string: iconImages[indexPath.row])!
           let resource = ImageResource(downloadURL: urlImage, cacheKey: "image")
           
           cell.iconImage.kf.setImage(with: resource)
           return cell
       }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:80 , height:80)
            }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
