//
//  DetailPageVCBuilder.swift
//  PhoneListApp
//
//  Created by faiq adi on 14/03/23.
//

import Foundation
import UIKit

class DetailPageVCBuilder: BaseViewController {
    
    var collectionView: UICollectionView!
    var cellId = "cellId"
    let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setupLayout()
    }
    func setupLayout(){
        layout.itemSize = CGSizeMake(110, 110)
        collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: layout)
        collectionView.programatically()
        collectionView.register(DetailCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        
        layout.scrollDirection = UICollectionView.ScrollDirection.vertical
        collectionView.setCollectionViewLayout(layout, animated: true)
        
        view.addSubview(headerContainer)
        view.addSubview(headerTitle)
        view.addSubview(backButton)
        view.addSubview(thumbnailImage)
        view.addSubview(saveCircle)
        view.addSubview(detailTitle)
        view.addSubview(detailDesc)
        view.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            headerContainer.heightAnchor.constraint(equalToConstant: 40),
            headerContainer.widthAnchor.constraint(equalToConstant: view.frame.width),
            headerContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            headerContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            headerTitle.centerYAnchor.constraint(equalTo: headerContainer.centerYAnchor),
            headerTitle.centerXAnchor.constraint(equalTo: headerContainer.centerXAnchor),
            backButton.centerYAnchor.constraint(equalTo: headerContainer.centerYAnchor),
            backButton.leadingAnchor.constraint(equalTo: headerContainer.leadingAnchor, constant: 10),
            backButton.heightAnchor.constraint(equalToConstant: 30),
            backButton.widthAnchor.constraint(equalToConstant: 70),
            
            thumbnailImage.topAnchor.constraint(equalTo: headerContainer.bottomAnchor, constant: 5),
            thumbnailImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            thumbnailImage.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            thumbnailImage.heightAnchor.constraint(equalToConstant: 250),
            
            saveCircle.topAnchor.constraint(equalTo: thumbnailImage.topAnchor, constant: 10),
            saveCircle.leadingAnchor.constraint(equalTo: thumbnailImage.leadingAnchor, constant: 10),
            saveCircle.heightAnchor.constraint(equalToConstant: 40),
            saveCircle.widthAnchor.constraint(equalToConstant: 40),
            
            detailTitle.topAnchor.constraint(equalTo: thumbnailImage.bottomAnchor, constant: 5),
            detailTitle.leadingAnchor.constraint(equalTo: thumbnailImage.leadingAnchor),
            
            detailDesc.topAnchor.constraint(equalTo: detailTitle.bottomAnchor, constant: 5),
            detailDesc.leadingAnchor.constraint(equalTo: thumbnailImage.leadingAnchor),
            detailDesc.trailingAnchor.constraint(equalTo: thumbnailImage.trailingAnchor),
            
            collectionView.topAnchor.constraint(equalTo: detailDesc.bottomAnchor, constant: 10),
            collectionView.leadingAnchor.constraint(equalTo: thumbnailImage.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: thumbnailImage.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10)
        ])
    }
    
    var backgroundContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.programatically()
        return view
    }()
    
    var saveCircle: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        view.setRadius(radius: 20)
        view.programatically()
        return view
    }()
    
    var headerContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.programatically()
        return view
    }()
    let headerTitle: UILabel = {
       let label = UILabel()
        label.programatically()
        label.text = "Detail page"
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        return label
    }()
    let backButton : UIButton = {
        let button = UIButton()
        button.programatically()
        button.backgroundColor = .systemOrange
        button.layer.cornerRadius = 5
        button.setTitle("Back", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        return button
    }()
    let thumbnailImage: UIImageView = {
        let image = UIImageView()
        image.programatically()
        image.backgroundColor = .lightGray
        return image
    }()
    
    let detailTitle: UILabel = {
       let label = UILabel()
        label.programatically()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        return label
    }()
    
    let detailDesc: UILabel = {
        let label = UILabel()
         label.programatically()
         label.text = ""
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
         return label
     }()
}
