//
//  BeginingVCBuilder.swift
//  testProject
//
//  Created by faiq adi on 23/02/23.
//

import Foundation
import UIKit

class BeginingVCBuilder: BaseViewController {
    
    var tableView : UITableView!
    var cellId = "cell"
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setupLayout()
    }
    func setupLayout(){
        tableView = UITableView()
        tableView.programatically()
        
        tableView.register(ListCell.self, forCellReuseIdentifier: cellId)
        
        tableView.separatorStyle = .singleLine
        
        
        view.addSubview(headerContainer)
        view.addSubview(headerTitle)
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            headerContainer.heightAnchor.constraint(equalToConstant: 40),
            headerContainer.widthAnchor.constraint(equalToConstant: view.frame.width),
            headerContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            headerContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            headerTitle.centerYAnchor.constraint(equalTo: headerContainer.centerYAnchor),
            headerTitle.centerXAnchor.constraint(equalTo: headerContainer.centerXAnchor),
            
            tableView.topAnchor.constraint(equalTo: headerContainer.bottomAnchor, constant: 10),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    var backgroundContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        view.programatically()
        return view
    }()
    
    var headerContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.programatically()
        return view
    }()
    let headerTitle: UILabel = {
       let label = UILabel()
        label.programatically()
        label.text = "Product List"
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        return label
    }()
}
