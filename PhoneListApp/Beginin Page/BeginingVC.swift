//
//  BeginingVC.swift
//  testProject
//
//  Created by faiq adi on 23/02/23.
//

import Foundation
import UIKit
import Kingfisher

class BeginingVC: BeginingVCBuilder{
    
    var presenter: VTPBeginingProtocol?
    
    var listData = [Product]()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        presenter?.getHitAPi()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
}
extension BeginingVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        cell?.preservesSuperviewLayoutMargins = false
        cell?.separatorInset = UIEdgeInsets.zero
        cell?.layoutMargins = UIEdgeInsets.zero
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ListCell else {return}
        cell.phoneLabel.text = listData[indexPath.row].title
        cell.phoneDesc.text = listData[indexPath.row].description
        let urlImage = URL(string: listData[indexPath.row].thumbnail)!
        let resource = ImageResource(downloadURL: urlImage, cacheKey: "imageCell")

        cell.phoneImage.kf.setImage(with: resource)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailPageVC()
        vc.descData = listData[indexPath.row].description
        vc.titleData = listData[indexPath.row].title
        vc.imageUrl = listData[indexPath.row].thumbnail
        vc.iconImages = listData[indexPath.row].images
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
}
extension BeginingVC: PTVBeginingProtocol{
    func showSuccessResponses(listData: [Product]){
        self.listData = listData
        tableView.reloadData()
    }
}
