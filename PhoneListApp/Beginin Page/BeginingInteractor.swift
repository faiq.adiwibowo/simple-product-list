//
//  File.swift
//  testProject
//
//  Created by faiq adi on 23/02/23.
//

import Foundation
import Alamofire

class BeginingInteractor: PTIBeginingProtocol {
    var presenter: ITPBeginingProtocol?
    
    func getHitAPI(){
        if let url = APIConstants.getLink(baseURL: .URL_BASE, mainEndpoint: .none, endpoints: [.products]) {
            AF.request(url).responseData { response in
                switch response.result {
                case .success(let data):
                    do {
                        let decoded = try JSONDecoder().decode(ListModel.self, from: data)
                        print("decoded data = \(decoded)")
                        self.presenter?.hitAPISuccess(listData: decoded.products)
                    } catch {
                        print("fail decoded")
                    }
                case .failure(let error):
                    print("fail \(error)")
                }
            }
            
        }
    }
}
