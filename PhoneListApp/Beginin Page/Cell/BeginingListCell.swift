//
//  BeginingListCell.swift
//  PhoneListApp
//
//  Created by faiq adi on 14/03/23.
//

import Foundation
import UIKit

class ListCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .blue
        
        addSubview(cellContainer)
        addSubview(phoneImage)
        addSubview(phoneLabel)
        addSubview(phoneDesc)
        
        NSLayoutConstraint.activate([
            cellContainer.leadingAnchor.constraint(equalTo: leadingAnchor),
            cellContainer.topAnchor.constraint(equalTo: topAnchor),
            cellContainer.trailingAnchor.constraint(equalTo: trailingAnchor),
            cellContainer.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            phoneImage.widthAnchor.constraint(equalToConstant: 80),
            phoneImage.heightAnchor.constraint(equalToConstant: 80),
            phoneImage.topAnchor.constraint(equalTo: cellContainer.topAnchor, constant: 10),
            phoneImage.bottomAnchor.constraint(equalTo: cellContainer.bottomAnchor, constant: -10),
            phoneImage.leadingAnchor.constraint(equalTo: cellContainer.leadingAnchor, constant: 10),
            
            phoneLabel.leadingAnchor.constraint(equalTo: phoneImage.trailingAnchor, constant: 12),
            phoneLabel.trailingAnchor.constraint(equalTo: cellContainer.trailingAnchor, constant: -12),
            phoneLabel.topAnchor.constraint(equalTo: phoneImage.topAnchor),
            
            phoneDesc.topAnchor.constraint(equalTo: phoneLabel.bottomAnchor, constant: 3),
            phoneDesc.leadingAnchor.constraint(equalTo: phoneLabel.leadingAnchor),
            phoneDesc.trailingAnchor.constraint(equalTo: phoneLabel.trailingAnchor),
            phoneDesc.bottomAnchor.constraint(equalTo: phoneImage.bottomAnchor)
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK:   COMPONENT
    let cellContainer: UIView = {
        let view = UIView()
        view.programatically()
        view.backgroundColor = .clear
        return view
    }()
    let phoneImage: UIImageView = {
        let image = UIImageView()
        image.programatically()
        image.backgroundColor = .lightGray
        return image
    }()
    let phoneLabel: UILabel = {
       let label = UILabel()
        label.programatically()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        return label
    }()
    let phoneDesc: UILabel = {
        let label = UILabel()
         label.programatically()
         label.text = ""
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
         return label
     }()
}
