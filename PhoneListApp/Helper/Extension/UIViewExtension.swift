//
//  UIViewExtension.swift
//  PhoneListApp
//
//  Created by faiq adi on 14/03/23.
//

import Foundation
import UIKit

extension UIView {
    
    func programatically(){
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    func setRadius(radius: CGFloat){
        self.layer.masksToBounds = false
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    func addBorder(color: UIColor){
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1
    }
}
