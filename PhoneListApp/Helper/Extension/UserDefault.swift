//
//  UserDefault.swift
//  PhoneListApp
//
//  Created by faiq adi on 14/03/23.
//

import Foundation

extension UserDefaults{
    
    enum UserDefaultIdentifier: String {
        case result
        case input
    }
    
    func setArrayStringValue(value: [String], identifier: UserDefaultIdentifier){
        set(value, forKey: identifier.rawValue)
    }
    func getArrayStringValue(identifier: UserDefaultIdentifier) -> [String]{
        return (object(forKey: identifier.rawValue) as? [String]) ?? [String]()
    }
}
