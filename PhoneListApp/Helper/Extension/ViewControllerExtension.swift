//
//  ViewControllerExtension.swift
//  PhoneListApp
//
//  Created by faiq adi on 14/03/23.
//

import Foundation
import UIKit

extension UIViewController {
    func showToast(_ label: UILabel, message: String, constaint: [NSLayoutConstraint]){
        
        let viewBackground = UIView()
        viewBackground.backgroundColor = .white
        viewBackground.addBorder(color: .gray)
        viewBackground.setRadius(radius: 5)
        
        viewBackground.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.textColor = UIColor.black
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center;
        
        label.font = UIFont.systemFont(ofSize: 15)
        //            .InterRegular(size: 12)
        label.text = message
        label.alpha = 1.0
        //        label.layer.cornerRadius = 10;
        label.clipsToBounds  =  true
        label.numberOfLines = 3
        
        self.view.addSubview(viewBackground)
        self.view.addSubview(label)
        label.isHidden = true
        viewBackground.isHidden = true
        
        NSLayoutConstraint.activate(constaint)
        NSLayoutConstraint.activate([label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
                                     label.widthAnchor.constraint(lessThanOrEqualTo: self.view.widthAnchor, constant: -40),
                                     label.heightAnchor.constraint(lessThanOrEqualToConstant: 100),
                                     viewBackground.widthAnchor.constraint(equalTo: label.widthAnchor, constant: 10),
                                     viewBackground.heightAnchor.constraint(equalTo: label.heightAnchor, constant: 10),
                                     viewBackground.centerXAnchor.constraint(equalTo: label.centerXAnchor),
                                     viewBackground.centerYAnchor.constraint(equalTo: label.centerYAnchor)])
        
        viewBackground.isHidden = false
        label.isHidden = false
        
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            label.alpha = 0.0
            viewBackground.alpha = 0.0
        }, completion: {(isCompleted) in
            label.removeFromSuperview()
        })
        
    }
}
