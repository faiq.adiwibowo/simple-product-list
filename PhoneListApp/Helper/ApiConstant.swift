//
//  ApiConstant.swift
//  PhoneListApp
//
//  Created by faiq adi on 14/03/23.
//

import Foundation
import Alamofire

struct APIConstants {
    
    enum BaseURL: String{
        case URL_BASE = "https://dummyjson.com/"
    }
    
    enum MainEndpoint: String{
        case phone
    }
    
    enum Endpoint: String{
        case products
    }
    
    struct Keys{
        static let response_code = "response_code"
        static let message = "message"
        static let data = "data"
        static let url = "url"
    }
    
    static func getLink(baseURL: BaseURL, mainEndpoint: MainEndpoint?, endpoints: [Endpoint]) -> URL?{
        do{
            var url = try baseURL.rawValue.asURL()
            
            if mainEndpoint != nil{
                url.appendPathComponent(mainEndpoint?.rawValue ?? "")
            }
            
            for endpoint in endpoints{
                url.appendPathComponent(endpoint.rawValue)
            }
            
            print(url)
            
            return url
        } catch _{
            return nil
        }
    }
    
}
